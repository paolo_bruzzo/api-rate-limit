"""
Decorator that limits the rate of the API calls
"""


def rate_limit(api_key, max_requests_per_10_seconds):
    from time import time as now
    from time import sleep
    from threading import Lock

    lock = Lock()  # to support multithreading

    # Decorator that takes the API function as input
    def decorator(func):
        calls = []  # Init

        # Wrapper executed every time before the actual function call
        def wrapped(*args, **kargs):
            lock.acquire()
            # Remove all the logs registered more than 10 seconds ago for this function
            for call in list(calls):
                if now() - call <= 10: break
                calls.remove(call)

            # If I have more than 'maxRequests' logs in the last 10 seconds, suspend
            if len(calls) >= max_requests_per_10_seconds:
                print api_key + " Suspended 5 minutes"
                sleep(5 * 60)  # suspended 5 minutes
                print api_key + " Available again"

            # Automatically restart the API when the suspension ends
            calls.append(now())
            lock.release()
            return func(*args, **kargs)

        return wrapped

    return decorator
