from Model.MyHTTPServer import MyHTTPServer
from sys import argv

if __name__ == '__main__':

    port = 0
    try:
        port = int(argv[1])
        if not 1 <= port <= 65535:
            raise ValueError("The port must be an integer between 1 and 65535")
    except ValueError:
        print "The port must be an integer"

    # Create server, and listen
    server = MyHTTPServer('localhost', port)
    server.listen()