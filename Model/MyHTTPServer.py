from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
from csv import reader as csv_reader
from urlparse import urlparse, parse_qs

from Model.API import API

MY_API = None


class MyHTTPServer:
    # =============================================================
    # CONSTRUCTORS
    # =============================================================
    def __init__(self, hostname='localhost', port=80):
        global MY_API

        # Init
        self.hostname = hostname
        self.port = port
        MY_API = API(csv_reader(open('Data/hoteldb.csv', 'rU')))

    # =============================================================
    # PUBLIC METHODS
    # =============================================================
    """Listen for HTTP requests"""
    def listen(self):
        server = self.ThreadedHTTPServer((self.hostname, self.port), self.Handler)
        print 'Listening for GET requests on http://' + self.hostname + ':' + str(self.port) + ', use <Ctrl-C> to quit'
        server.serve_forever()

    # =============================================================
    # INNER CLASSES
    # =============================================================
    class Handler(BaseHTTPRequestHandler):

        # Store all the parameter in a dictionary
        def _get_http_parameters(self, url):
            params = parse_qs(urlparse(url).query)
            return {key: params[key][0] for key in params}

        # @override
        def do_GET(self):
            global MY_API
            self.send_response(200)
            self.end_headers()
            params = self._get_http_parameters(self.path)

            # Don't consider any GET request that do not contain an api key
            if 'key' not in params:
                self.wfile.write('Please specify the key field\n')
                return

            # Call the API and respond with the result
            self.wfile.write(MY_API.call(params) + '\n')
            return

    class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
        pass  # To handle in different threads
