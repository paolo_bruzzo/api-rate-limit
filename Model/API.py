from Utils.RateLimit import rate_limit

class API:
    # =============================================================
    # DEFINES / CONFIGURATIONS
    # =============================================================
    # Api Keys
    SEARCH_BY_CITY_ID = "SEARCH_BY_CITY_ID"
    SOMETHING_ELSE = "SOMETHING_ELSE"  # Useful to test more than 1 api_key

    # Configuration
    GLOBAL_RATE_LIMIT = 100  # max requests per 10 seconds
    rate_limits = {  # max requests per 10 seconds
        SEARCH_BY_CITY_ID: 10,  # comment this line to test the global rate limit
        SOMETHING_ELSE: 25
    }

    # =============================================================
    # CONSTRUCTORS
    # =============================================================
    def __init__(self, data):
        self.header = next(data)  # remove header
        self.data = [[row[0], int(row[1]), row[2], int(row[3])] for row in data]  # cast types

    # =============================================================
    # PUBLIC METHODS
    # =============================================================
    """ Public interface for the API calls """

    def call(self, params):
        api_key = params['key']
        del params['key']

        if api_key == API.SEARCH_BY_CITY_ID: return self._search_by_city_id(params)
        if api_key == API.SOMETHING_ELSE: return self._search_by_something_else(params)

        return api_key + " is not a valid api call"

    # =============================================================
    # PRIVATE METHODS
    # =============================================================

    # To pretty print the resulting list
    def _pretty_layout(self, data):
        return ','.join(self.header) + '\n' + '\n'.join([', '.join(map(str, row)) for row in data])

    @rate_limit(SEARCH_BY_CITY_ID, rate_limits[SEARCH_BY_CITY_ID] if SEARCH_BY_CITY_ID in rate_limits else GLOBAL_RATE_LIMIT)
    def _search_by_city_id(self, params):
        # Check if city_id is included in the parameters (Mandatory)
        if 'city_id' not in params:
            return 'Please provide the \'city_id\' parameter'
        result = filter(lambda row: row[0].lower() == params['city_id'].lower(), self.data)
        # Check if sort is included in the parameters (Optional)
        if 'sort' not in params:
            return self._pretty_layout(result)  # No sorting
        if params['sort'] == 'ASC':
            return self._pretty_layout(sorted(result, key=lambda row: row[3]))  # ASC sorting
        if params['sort'] == 'DESC':
            return self._pretty_layout(sorted(result, key=lambda row: row[3], reverse=True))  # DESC sorting
        return "The sort parameter can only be \'ASC\' or \'DESC\' if present"

    @rate_limit(SOMETHING_ELSE, rate_limits[SOMETHING_ELSE] if SOMETHING_ELSE in rate_limits else GLOBAL_RATE_LIMIT)
    def _search_by_something_else(self, something_else):
        return "Called something else"
