# README #


### How do I use the script ? ###

* Download the [Repository](https://bitbucket.org/paolo_bruzzo/api-rate-limit/downloads?tab=downloads)
* Unzip it, cd into it, and type in the console:$ **python Main.py 8080** (this will run an HTTP server on your localhost, on port 8080. Change the port value at your convenience.)
* Open a URL such as **http://localhost:8080/?key=SEARCH_BY_CITY_ID&city_id=Amsterdam&sort=ASC** on your web browser (Google Chrome suggested)
* Replacing 'Amsterdam' with 'Bangkok', changing the sort to 'DESC' or removing it from the URL, will display the result filtered from the original CSV.
* Sending more than 10 requests in 10 seconds (current configuration for SEARCH_BY_CITY_ID api key), will suspend it for 5 minutes (leaving of course SOMETHING_ELSE api_key still accessible)
* Optional: go to the Model/API.py file to change the rate limit configuration if you wish (from line 12). This operation requires a reboot of the server.

### Requirements ###
* Python 2.7, a web browser, and/or any other tool to generate HTTP GET requests

### Assumptions based on the description ###
* The Rate Limit, checks the number of requests done in the last 10 seconds from now. So an API function with a rate limit of 1000 requests in 10 seconds, can be called 1000 times within 1 second and 0 times within the following 9 seconds, without being suspended.
* This program does not implement any DOS attack protection. If someone sends 3 million requests within a few seconds (e.g. from 3 million different threads) to the same api_key, the server will respond to the first N until the rate limit is reached and suspends that api_key for 5 minutes. This process is automatically repeated until all the requests have been handled.
* Given the small CSV provided, all the data is kept in memory. This program should be extended to interface with a database in case one wants to handle a much bigger dataset.